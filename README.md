# Java_ClassMaterial_072018

==================================================================================================================================
Java Reading Material# 

Java Docs - https://docs.oracle.com/javase/8/docs/
Ultra Basic Java (Free- Good for brand new learner to start) - http://greenteapress.com/thinkjava6/thinkjava.pdf
Core Java Book (Recommended) - https://www.amazon.com/Head-First-Java-Brain-Friendly-Guide-ebook/dp/B009KCUX3S/ref=mt_kindle?_encoding=UTF8&me=&qid=
Java Notes (Free Site) - http://math.hws.edu/javanotes/contents-with-subsections.html
Java by ntu.edu- https://www.ntu.edu.sg/home/ehchua/programming/

# Java Coding Practice
===================================================================================================================================
Basic Java coding practice - http://codingbat.com/java 
Code Excercises - http://code-exercises.com/
Coding challenge - https://www.hackerrank.com/domains/java
LeetCode - https://leetcode.com/problemset/all/


# Download & Install--
==================================================================================================================================
JDK installation - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html?ssSourceSiteId=otnpt

Spring Toolsuite [IDE] - https://spring.io/tools/sts/all

Notepad++ - https://notepad-plus-plus.org/download/v7.5.7.html

Git - https://git-scm.com/downloads

SoapUI - https://www.soapui.org/

Maven - Download Zip file - https://maven.apache.org/download.cgi
		https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html




